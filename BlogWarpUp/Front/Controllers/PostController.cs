﻿using Back.model;
using System.Net;
using Front.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Front.Controllers
{
    public class PostController : Controller
    {
        string url = "http://localhost:8081/api/posteo";
        public async Task<IActionResult> ListPosts()
        {
            //C: \Users\Blackhorde\source\repos\BlogWarpUp
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync(url );
            List<Post> posts = JsonConvert.DeserializeObject<List<Post>>(json);
            return View(posts);
        }
        [HttpGet]
        public ActionResult AddPost()
        {
            Post p = new Post();
            p.FechaCreacion = DateTime.Now;
            return View(p);
        }

        [HttpPost]
        public async Task<ActionResult> AddPost(Post p)
        {
            p.Activo = true;
            var httpClient = new HttpClient();
            var data = System.Text.Json.JsonSerializer.Serialize(p);
            HttpContent content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
            var httpResponse = await httpClient.PostAsync(url, content);
            
            if (httpResponse.IsSuccessStatusCode)
            {
                var result = await httpResponse.Content.ReadAsStringAsync();
            }

            var json = await httpClient.GetStringAsync(url);
            List<Post> posts = JsonConvert.DeserializeObject<List<Post>>(json);
            return View("ListPosts", posts);
        }

        public async Task<ActionResult> EditPost(int id)
        {
            var httpClient = new HttpClient();
            string url2 = "http://localhost:8081/api/posteo/" + id;
            var json = await httpClient.GetStringAsync(url2);
            Post posts = JsonConvert.DeserializeObject<Post>(json);
            return View(posts);

        }
        [HttpPost]
        public async Task<ActionResult> EditPost(Post p)
        {
            int id = p.Id;
            p.Activo = true;
            string url2 = "http://localhost:8081/api/posteo/" + id;
            var httpClient = new HttpClient();
            var data = JsonConvert.SerializeObject(p);

            HttpContent content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
            var httpResponse = await httpClient.PatchAsync(url2, content);

            if (httpResponse.IsSuccessStatusCode)
            {
                var result = await httpResponse.Content.ReadAsStringAsync();
            }

            var json = await httpClient.GetStringAsync(url);
            List<Post> posts = JsonConvert.DeserializeObject<List<Post>>(json);
            return View("ListPosts", posts);

        }
        public async Task<ActionResult> Delete(int id)
        {
            var httpClient = new HttpClient();
            string url2 = "http://localhost:8081/api/posteo/" + id;
            var httpResponse = await httpClient.DeleteAsync(url2);
            if (httpResponse.IsSuccessStatusCode)
            {
                var result = await httpResponse.Content.ReadAsStringAsync();
            }
            var json2 = await httpClient.GetStringAsync(url);
            List<Post> posts = JsonConvert.DeserializeObject<List<Post>>(json2);
            return View("ListPosts" ,posts);
        }

        public async Task<ActionResult> View(int id)
        {
            var httpClient = new HttpClient();
            string url2 = "http://localhost:8081/api/posteo/" + id;
            var json = await httpClient.GetStringAsync(url2);
            Post posts = JsonConvert.DeserializeObject<Post>(json);
            return View(posts);
        }
    }
}
