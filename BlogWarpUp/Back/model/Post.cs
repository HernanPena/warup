﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Back.model
{
    public partial class Post
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Contenido { get; set; }
        public string Urlimagen { get; set; }
        public int Categoria { get; set; }
        public DateTime FechaCreacion { get ; set; }
     
        public bool? Activo { get; set; }
    }
}
