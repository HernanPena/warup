﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Back.model
{
    public partial class Categorium
    {
        public int Id { get; set; }
        public string Categoria { get; set; }
    }
}
