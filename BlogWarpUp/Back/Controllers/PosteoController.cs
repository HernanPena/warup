﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using Back.model;


namespace Back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PosteoController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<Post>> get()
        {
            try
            {
                using (var bd = new model.BlogWarpUpContext())
                {
                    IEnumerable<Post> posts = (from d in bd.Posts
                                               orderby d.FechaCreacion descending
                                               select new model.Post
                                               {
                                                   Id = d.Id,
                                                   Titulo = d.Titulo,
                                                   Urlimagen = d.Urlimagen,
                                                   Categoria = d.Categoria,
                                                   FechaCreacion = d.FechaCreacion
                                               }).ToList();

                    return posts.ToList();
                }
            }
            catch
            {
                return NotFound();
            }

        }
        [HttpGet("{id}")]
        public ActionResult<Post> get(int id)
        {
            try
            {
                using (var bd = new model.BlogWarpUpContext())
                {
                    var po = from p in bd.Posts
                             where p.Id == id
                             select new model.Post
                             {
                                 Id = p.Id,
                                 Titulo = p.Titulo,
                                 FechaCreacion =Convert.ToDateTime(p.FechaCreacion.ToString("yyyy/MM/dd")),
                                 Contenido = p.Contenido,
                                 Urlimagen = p.Urlimagen,
                                 Categoria = p.Categoria,
                                 Activo = p.Activo
                             };

                    model.Post post = po.First();
                    if (post != null)
                    {
                        return post;
                    }
                    else
                    {
                        return BadRequest();
                    }
                }

            }
            catch
            {
                return NotFound();
            }

        }
        [HttpPatch("{id}")]

        public ActionResult update(int id, Post p)
        {
            try 
            {
                using (var db = new BlogWarpUpContext())
                {
                    Post post = db.Posts.Find(id);
                    post.Activo = p.Activo;
                    post.Categoria = p.Categoria;
                    post.Contenido = p.Contenido;
                    post.FechaCreacion = p.FechaCreacion;
                    post.Titulo = p.Titulo;
                    post.Urlimagen = p.Urlimagen;
                    db.Entry(post).State = EntityState.Modified;
                    db.SaveChanges();
                    return NoContent();
                }
            }
            catch 
            {
                return NotFound();
            }
        }
        [HttpDelete ("{id}")]
        public ActionResult delete(int id)
        {
            try
            {
                using (var db = new BlogWarpUpContext())
                {
                    Post post = db.Posts.Find(id);
                    db.Posts.Remove(post);
                    db.SaveChanges();
                    return NoContent();
                }
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        public ActionResult add_post(Post post)
        {
            try
            {
                using (var db = new BlogWarpUpContext())
                {
                    db.Posts.Add(post);
                    db.SaveChanges();
                    return NoContent();
                }
            }
            catch
            {
                return NotFound();
            }

        }

    }
}
